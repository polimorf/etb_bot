from app.models import Auction, Category, State, ScraperLog
from app import db
from datetime import datetime
import json
import requests
import traceback

# API URLs
categories_url = "https://sale.tbe-birzha.com.ua/chatbot/category"
items_url_template = "https://sale.tbe-birzha.com.ua/chatbot/item?status=active_tendering&category={category}&page={page}"


def retrieve_list_of_categories():
    response = requests.get(categories_url)
    return json.loads(response.text)


# Function to retrieve items for a specific category and page
def get_items_for_category(category, page=1):
    url = items_url_template.format(category=category, page=page)
    response = requests.get(url)
    return json.loads(response.text)


def retrieve_all_items():
    try:
        all_auctions_items = {}
        categories = retrieve_list_of_categories()
        items_amount = 0
        for category in categories:
            page = 1
            while True:
                items_data = get_items_for_category(category, page)
                if 'items' in items_data and len(items_data['items']) > 0:
                    if category not in all_auctions_items:
                        all_auctions_items[category] = []
                    all_auctions_items[category].extend(items_data['items'])
                    items_amount += len(items_data['items'])
                    if page >= items_data['totalPages'] or page > 2:
                        break
                    page += 1
                else:
                    break
        log_info(""+str(items_amount)+" items was getting from tbe API")
        return all_auctions_items
    except:
        log_error(6)


def is_not_added(auction_id):
    auction = Auction.query.filter_by(aid=auction_id).first()
    return auction is None


def get_additional_info(auction_item):
    try:
        if "GFE0" in auction_item["auctionId"]:
            api_url = "https://dgf-procedure.prozorro.sale/api/search/byAuctionId/{auction_id}"
        else:
            api_url = "https://procedure.prozorro.sale/api/search/byAuctionId/{auction_id}"

        auction_item["erdpou"] = "000000"
        auction_item["cpv"] = "0-000000"
        auction_item["region"] = "NONE"

        url = api_url.format(auction_id=auction_item["auctionId"])
        response = requests.get(url)
        extended_info = json.loads(response.text)

        is_erdpou = False
        if "sellingEntity" in extended_info:
            if "identifier" in extended_info["sellingEntity"]:
                if "id" in extended_info["sellingEntity"]["identifier"]:
                    auction_item["erdpou"] = extended_info["sellingEntity"]["identifier"]["id"]
                    is_erdpou = True

        if not is_erdpou and "relatedOrganizations" in extended_info:
            if "propertyOwner" in extended_info["relatedOrganizations"]:
                if "identifier" in extended_info["relatedOrganizations"]["propertyOwner"]:
                    if "id" in extended_info["relatedOrganizations"]["propertyOwner"]["identifier"]:
                        auction_item["erdpou"] = extended_info["relatedOrganizations"]["propertyOwner"]["identifier"][
                            "id"]

        if "items" in extended_info:
            if len(extended_info["items"]) > 0:
                if "classification" in extended_info["items"][0]:
                    if "id" in extended_info["items"][0]["classification"]:
                        auction_item["cpv"] = extended_info["items"][0]["classification"]["id"]

        is_address = False
        if "items" in extended_info:
            if len(extended_info["items"]) > 0:
                if "address" in extended_info["items"][0]:
                    if "region" in extended_info["items"][0]["address"]:
                        if "uk_UA" in extended_info["items"][0]["address"]["region"]:
                            auction_item["region"] = extended_info["items"][0]["address"]["region"]["uk_UA"]
                            is_address = True

        if not is_address and "sellingEntity" in extended_info:
            if "address" in extended_info["sellingEntity"]:
                if "region" in extended_info["sellingEntity"]["address"]:
                    if "uk_UA" in extended_info["sellingEntity"]["address"]["region"]:
                        auction_item["region"] = extended_info["sellingEntity"]["address"]["region"]["uk_UA"]
    except:
        log_error(4)
    finally:
        return auction_item


def get_category_id(category_short_name, cpv_code):
    try:
        category_id = 12
        #category_obj = Category.query.filter_by(name=category_short_name).first()
        #if category_obj is not None:
        #    category_id = category_obj.id
        #else:

        categories_map = Category.query.filter_by(site=1).all()
        for category_obj in categories_map:
            if category_obj.name is not None:
                cat_arr = category_obj.name.split(",")
                stripped = [s.strip() for s in cat_arr]
                if category_short_name in stripped:
                    category_id = category_obj.id
                    break

        if category_id == 12:
            for category_obj in categories_map:
                if category_obj.cpv_codes is not None:
                    cat_arr = category_obj.cpv_codes.split(",")
                    stripped = [s.strip() for s in cat_arr]
                    if cpv_code in stripped:
                        category_id = category_obj.id
                        break

    except:
        log_error(5)
    finally:
        return category_id


def get_state_id(state_name):
    states = State.query.all()
    state_id = 1
    for st in states:
        if st.pars_name in state_name:
            state_id = st.id
            break

    return state_id


def adding_new_items_to_db(scraped_items):
    try:
        for category_name in scraped_items.keys():
            category_items = scraped_items[category_name]
            items_added = 0
            for item in category_items:
                if is_not_added(item["auctionId"]):
                    items_added += 1
                    extended_item = get_additional_info(item)

                    auction = Auction()
                    auction.title = extended_item["title"][:1000]
                    auction.aid = extended_item["auctionId"]
                    auction.cpv_code = extended_item["cpv"]
                    auction.url = extended_item["link"]
                    auction.erdpou = extended_item["erdpou"]
                    auction.state_id = get_state_id(extended_item["region"])
                    auction.category_id = get_category_id(category_name, extended_item["cpv"])

                    db.session.add(auction)
                    db.session.commit()
            if items_added > 0:
                log_info("New " + str(items_added) + " items from category: " + category_name + " was added")
    except:
        log_error(3)


def log_error(log_type):
    log = ScraperLog()
    log.result_code = log_type
    message = traceback.format_exc()
    print("Exception: " + message)
    log.error = message[:2040]
    log.up_date = datetime.now()
    db.session.add(log)
    db.session.commit()


def log_info(message):
    print(message)
    log = ScraperLog()
    log.result_code = 0
    log.error = message[:2040]
    log.up_date = datetime.now()
    db.session.add(log)
    db.session.commit()


if __name__ == '__main__':
    try:
        all_items = retrieve_all_items()
        adding_new_items_to_db(all_items)
    except:
        log_error(2)
