var app = new Vue({
        el: '#app',
        delimiters: ['[[', ']]'],
        data() {
            return {
                pages: [],
                subscribers: [],
            }
        },
        methods: {
            getPage: function (page) {
                axios.get('/api/v1/subscribers/'+page)
                .then(function (responce) {
                    app.$data.subscribers = responce.data.subscribers;
                    app.$data.pages = responce.data.pages;
                })
                .catch(function (error) {
                    console.log(error);
                })
            }
        },
        computed:
            {}
        ,
        mounted() {
            axios.get('/api/v1/subscribers/1')
                .then(function (responce) {
                    app.$data.subscribers = responce.data.subscribers;
                    app.$data.pages = responce.data.pages;
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    })
;

$(document).ready(function () {
    console.log('subscribers is ready');
});



