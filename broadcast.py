import telebot
from app.models import Auction, ThemeErdpou, ThemeCPV, Theme, ScraperLog
from sqlalchemy import desc
from datetime import datetime
from app import db
from tbebot import bot
import time


def send_to_user(sub):
    if sub.state.id > 1:
        auctions = Auction.query.filter(Auction.category_id == sub.category.id, Auction.state_id == sub.state.id,
                                        Auction.created_date > sub.last_update)
    else:
        auctions = Auction.query.filter(Auction.category_id == sub.category.id, Auction.state_id >= 1,
                                        Auction.created_date > sub.last_update)

    cnt = 1
    if auctions.count() > 0:
        message_text = '*' + sub.category.title + ' - ' + (
            sub.state.name + '*' if sub.state_id == 1 else sub.state.name + ' область*') + '\n'
        for auction in auctions:
            state_info = ''
            if auction.state_id > 1:
                state_info = '(' + auction.state.name + ' область)'
            message_text += str(cnt) + '. [' + ((auction.title[:30] + '..]') if len(
                auction.title) > 30 else '[' + auction.title + ']') + '(' + auction.url + ') ' + state_info + '\n'
            cnt += 1
            if cnt > 5:
                break

        try:
            bot.send_message(sub.subscriber.chat_id, text=message_text)
        except Exception as e:
            print(e)
        finally:
            sub.last_update = datetime.now()
            db.session.commit()


def send_erdpou(sub):
    if sub.state.id > 1:
        auctions = Auction.query.filter(Auction.erdpou == sub.erdpou, Auction.state_id == sub.state_id,
                                        Auction.created_date > sub.last_update)
    else:
        auctions = Auction.query.filter(Auction.erdpou == sub.erdpou, Auction.state_id >= 0,
                                        Auction.created_date > sub.last_update)

    cnt = 1
    if auctions.count() > 0:
        message_text = '*ЄДРПОУ: ' + sub.erdpou + '-' + (
            sub.state.name + '*' if sub.state_id == 1 else sub.state.name + ' область*') + '\n'
        for auction in auctions:
            state_info = ''
            if auction.state_id > 1:
                state_info = '(' + auction.state.name + ' область)'
            message_text += str(cnt) + '. [' + ((auction.title[:20] + '..]') if len(
                auction.title) > 20 else '[' + auction.title + ']') + '(' + auction.url + ') ' + state_info + '\n'
            cnt += 1
            if cnt > 5:
                break

        try:
            bot.send_message(sub.subscriber.chat_id, text=message_text)
        except Exception as e:
            print(e)
        finally:
            sub.last_update = datetime.now()
            db.session.commit()


def send_cpv(sub):
    if sub.state.id > 1:
        auctions = Auction.query.filter(Auction.cpv_code == sub.cpv, Auction.state_id == sub.state_id,
                                        Auction.created_date > sub.last_update)
    else:
        auctions = Auction.query.filter(Auction.cpv_code == sub.cpv, Auction.state_id >= 0,
                                        Auction.created_date > sub.last_update)

    cnt = 1
    if auctions.count() > 0:
        message_text = '*CPV: ' + sub.cpv + '-' + (
            sub.state.name + '*' if sub.state_id == 1 else sub.state.name + ' область*') + '\n'
        for auction in auctions:
            state_info = ''
            if auction.state_id > 1:
                state_info = '(' + auction.state.name + ' область)'
            message_text += str(cnt) + '. [' + ((auction.title[:50] + '..]') if len(
                auction.title) > 50 else '[' + auction.title + ']') + '(' + auction.url + ') ' + state_info + '\n'
            cnt += 1
            if cnt > 5:
                break

        try:
            bot.send_message(sub.subscriber.chat_id, text=message_text)
        except Exception as e:
            print(e)
        finally:
            sub.last_update = datetime.now()
            db.session.commit()


if __name__ == '__main__':

    try:
        log = ScraperLog.query.order_by(desc(ScraperLog.up_date)).limit(1).first()
        if log:
            subs = Theme.query \
                .filter(Theme.last_update < log.up_date) \
                .order_by(desc(Theme.last_update)).limit(100).all()

            erdpou = ThemeErdpou.query \
                .filter(ThemeErdpou.last_update < log.up_date) \
                .order_by(desc(ThemeErdpou.last_update)).limit(100).all()

            cpv = ThemeCPV.query \
                .filter(ThemeCPV.last_update < log.up_date) \
                .order_by(desc(ThemeCPV.last_update)).limit(100).all()

            for sub in subs:
                time.sleep(1)
                send_to_user(sub)

            for e in erdpou:
                time.sleep(1)
                send_erdpou(e)

            for c in cpv:
                time.sleep(1)
                send_cpv(c)
    except Exception as e:
        print(e)
