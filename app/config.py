import os


class Config(object):
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:vEzcom6i3P@127.0.0.1:8888/tbe'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'vEzcom6i3P'
