#! /usr/bin/env python
# -*- coding: utf-8 -*-


import telebot
from app.models import Auction, Category, State, Subscriber, Theme, ThemeErdpou, ThemeCPV, Comments
from app import db
from datetime import datetime
import os
import re
import time
from sqlalchemy import desc

#bot = telebot.TeleBot("1314680265:AAHFPs0PUYYsurLWJwL8-2k_ijz2j42HSb0", parse_mode="MARKDOWN")
bot = telebot.TeleBot("1305638718:AAG0ax_YmtMeWMxM0wCkwZp2p-EbiRcR5os", parse_mode="MARKDOWN")  # test token

botPath = os.path.dirname(os.path.abspath(__file__))
imagesDir = os.path.join(botPath, 'static', 'upload', 'images')

states = ['всі регіони', 'Вінницька', 'Волинська', 'Дніпропетровська',
          'Донецька', 'Житомирська', 'Закарпатська', 'Запорізька', 'Івано-Франківська',
          'Київська', 'Кіровоградська', 'Луганська',
          'Львівська', 'Миколаївська', 'Одеська',
          'Полтавська', 'Рівненська', 'Сумська', 'Тернопільська', 'Харківська', 'Херсонська',
          'Хмельницька', 'Черкаська', 'Чернігівська', 'Чернівецька']

categories = ['\U0001F697Авто', '\U0001F3D8Нерухомість', '\U0001F3D9Оренда',
              '\U0001F4C3Мала приватизація', '\U0001F527Металобрухт', '\U0001F682Вагони',
              '\U0001F30FЗемельні ділянки', '\U0001F4B0Користування надрами',
              '\U0001F4C1Велика приватизація', '\U0001F3DAПродаж майна банкрутів',
              '\U0001F333Необроблена деревина', '\U0001F30AПромисел - Право використання водних біоресурсів']

faq = ['Що потрібно для участі в аукціоні?',
       'Як зареєструватися?',
       'Які документи потрібні для реєстрації?',
       'Як подати заявку / пропозицію на аукціон?',
       'Як подивитися приміщення/авто, ознайомитися із документами?',
       'Які документи потрібні для участі в аукціоні?',
       'Чи можна оплачувати гарантійний внесок через касу/ термінал/ з картки іншої людини?',
       'Де знайти інформацію про дату та час аукціону?',
       'Як зайти на аукціон?',
       'Як проходить аукціон?']


@bot.message_handler(commands=['start', 'main'])
def send_welcome(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
    user_markup.row('\U0001F4CEПідписатися', '\U0001F50EПошук')
    user_markup.row('\U0001F5D1Видалити підписку', '\U0001F4C3Показати підписку')
    user_markup.row('\U0001F198FAQ', '\U0001F4B0Тарифи')
    user_markup.row('\u2B50Ваші відгуки', 'Про Прозоро Продажі')
    bot.send_message(message.from_user.id, 'Виберіть будь ласка зі списку...', reply_markup=user_markup)
    user = Subscriber.query.filter_by(chat_id=message.from_user.id).first()
    if user is None:
        user = Subscriber()
        user.chat_id = message.from_user.id
        if message.from_user.username:
            user.username = message.from_user.username
        else:
            user.username = message.from_user.name
        db.session.add(user)
    else:
        user.t_erdpou = None
        user.t_cpv = None
        user.t_comment = 0
    db.session.commit()


@bot.message_handler(content_types=['text'])
def menu_handler(message):
    try:
        db.session.commit()
        user = Subscriber.query.filter_by(chat_id=message.from_user.id).first()
        if message.text == '\U0001F519Головне меню':
            send_welcome(message)
        elif message.text == '\U0001F4CEПідписатися':
            click_subscribe(message)
        elif message.text == '\U0001F50EПошук' and user:
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, 'Останні аукціони з вашої підписці:', reply_markup=user_markup)

            subs = Theme.query \
                .filter(Theme.subscriber_id == user.id) \
                .order_by(desc(Theme.last_update)).limit(10).all()

            erdpou = ThemeErdpou.query \
                .filter(ThemeErdpou.subscriber_id == user.id) \
                .order_by(desc(ThemeErdpou.last_update)).limit(10).all()

            cpv = ThemeCPV.query \
                .filter(ThemeCPV.subscriber_id == user.id) \
                .order_by(desc(ThemeCPV.last_update)).limit(10).all()

            for sub in subs:
                time.sleep(1)
                find_subscribe(sub)

            for e in erdpou:
                time.sleep(1)
                find_erdpou(e)

            for c in cpv:
                time.sleep(1)
                find_cpv(c)



        elif message.text == '\U0001F5D1Видалити підписку':
            show_subscribes(message, user, 2)
        elif message.text == '\U0001F4C3Показати підписку':
            show_subscribes(message, user, 1)
        elif message.text == '\U0001F198FAQ':
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            for f in faq:
                user_markup.row(f)
            bot.send_message(message.from_user.id, 'Список найчастіших питань', reply_markup=user_markup)
        elif message.text == '\U0001F4B0Тарифи':
            click_prices(message)
        elif message.text == '\u2B50Ваші відгуки':
            user_markup = telebot.types.ReplyKeyboardRemove(selective=False)
            bot.send_message(message.from_user.id,
                             '\u2B50"Напишіть нам": Будь ласка, напишіть у текстовому повідомленні (як відповідь) свої пропозиції та зауваження.',
                             reply_markup=user_markup)
            user.t_comment = message.message_id
            db.session.commit()
        elif message.text == 'Про Прозоро Продажі':
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id,
                             '*Про Прозоро.Продажі*\n  Відкритість, прозорість, ефективність. Так трьома словами можна охарактеризувати проект, а тепер уже державне підприємство «ProZorro.Продажі», який успішно стартував у 2016 році. Завдяки дворівневій системі  - центральна база даних Прозорро та акредитовані електронні майданчики – платформа не лише забезпечує  чесні та відкриті аукціони, а й, головне, унеможливлює корупцію. Усі лоти виставляють та реєструються учасники аукціонів самостійно виключно через електронні майданчики. У 2016-му до нового проекту приєднався Фонд гарантування вкладів фізичних осіб – продавав на аукціонах майно неплатоспроможних банків. Результат не забарився – за півроку ФГВФО отримав перший мільйон. Тому було вирішено усі активи неплатоспроможних банків продавати через Прозорро. У 2017-му на Прозорро зайшли Фонд держмайна та органи місцевого самоврядування, які реалізовували майно своїх підприємств. Українська універсальна біржа – акредитований електронний майданчик системи Прозорро – одна із найдосвідченіших платформ, яка супроводжує від початку і до кінця у питаннях купівлі-продажу будь-яких товарів та послуг. Нині на майданчику проходять торги за кількома категоріями – мала приватизація, дозволи на користування надрами, права вимоги за кредитами, продаж нерухомості, автомобілів, деревини, металобрухту, оренда вагонів та нерухомості, інші комерційні торги. Хоча майданчики готові і до розширення асортименту ринку. Найближчим часом, обіцяють урядовці, на майданчиках з’являться майно банкрутів, конфісковане та арештоване виконавцями судових рішень майно, резерви аграрного фонду, списані ресурси державного резерву, земельні ділянки та спиртова продукція. Система Прозорро отримала міжнародне визнання. У 2017 році проект став одним із кращих антикорупційних стартапів світу. А у 2018 році система отримала антикорупційну нагороду від C5 Accelerate та USA Institute of Peace в рамках конкурсу “The Shield in the Cloud Innovation Challenge”.',
                             reply_markup=user_markup)
        elif message.text in categories:
            select_category(message, user)
        elif message.text == '\U0001F522ЄРДПОУ':
            input_erdpou(message, user)
            pass
        elif message.text == '\U0001F522CPV':
            input_cpv(message, user)
            pass
        elif message.text == '\U0001F504інші':
            select_state(message, 1)
        elif message.text == '\U0001F504ще':
            select_state(message, 2)
        elif message.text == '\U0001F502назад':
            select_state(message, 0)
        elif message.text in states:
            make_subscribe(message, user)
        elif ' - ' in message.text:
            remove_subscribe(message, user)
        elif 'ЦБД1 (продаж майна неплатоспроможних банків та НБУ)' == message.text:
            _text = 'Розмір грошової винагороди оператора за придбаний актив неплатоспроможного банку та/або банку, який перебуває у процесі ліквідації встановлюється у відсотковому відношенні до ціни продажу та складає:'
            photo = open(os.path.join(imagesDir, 'cbd1.jpg'), 'rb')
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo)
        elif 'ЦБД2 (продаж майна державної та комунальної форми власності)' == message.text:
            _text1 = 'Розмір винагороди оператора електронного майданчика, за придбане на аукціонах майно встановлюється у відсотковому відношенні до ціни продажу та складає:'
            photo1 = open(os.path.join(imagesDir, 'cbd2_1.jpg'), 'rb')

            _text2 = 'За участь у електронному аукціоні учасники сплачують реєстраційний внесок у наступному розмірі:'
            photo2 = open(os.path.join(imagesDir, 'cbd2_2.jpg'), 'rb')

            _text3 = 'Раєстраційний внесок -10 неоп.мінімумів(= 170 грн єдиний розмір для всіх аукціонів). Гарантійний внесок -5 % від стартової ціни продажу. Винагорода оператора - 5 % від ціни продажу лоту(flat rate)'

            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text1,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo1)
            bot.send_message(message.from_user.id, _text2,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo2)
            bot.send_message(message.from_user.id, _text3,
                             reply_markup=user_markup)

        elif 'ЦБД2 (продаж майна приватної форми власності)' == message.text:
            _text = 'Розмір винагороди оператора електронного майданчика, за придбане на аукціонах майно встановлюється у відсотковому відношенні до ціни продажу та складає:'
            photo = open(os.path.join(imagesDir, 'cbd2_3.jpg'), 'rb')
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo)
        elif 'Продаж майна АТ КБ "ПРИВАТБАНК"' == message.text:
            _text = 'Розмір винагороди оператора електронного майданчика, за придбане на аукціонах майно встановлюється у відсотковому відношенні до ціни продажу та складає:'
            photo = open(os.path.join(imagesDir, 'privat.jpg'), 'rb')
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo)
        elif 'Оренда майна державної, комунальної та приватної форм власності' == message.text:
            _text = "Розмір грошової винагороди оператора за придбаний лот встановлюється у відсотковому відношенні до ціни продажу та складає 5 (п'ять) відсотків річної орендної плати за результатами електронного аукціону."
            photo = open(os.path.join(imagesDir, 'orenda.jpg'), 'rb')
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo)
        elif 'Мала приватизація державного і комунального майна' == message.text:
            _text1 = "Розмір грошової винагороди оператора за придбаний об'єкт малої приватизації встановлюється у відсотковому відношенні до ціни продажу та складає:"
            photo1 = open(os.path.join(imagesDir, 'small_privat.jpg'), 'rb')
            _text2 = 'За реєстрацію заяви на участь у приватизації учасник сплачує реєстраційний внесок у розмірі 0,2 мінімальної заробітної плати станом на 1 січня поточного року. У 2021 році реєстраційний внесок складає 1 200,00 грн.'
            _text3 = 'ТБ «Європейська» приєдналася до Правил професійної поведінки Оператора електронного майданчика, що підключений до Електронної торгової системи Prozorro.Продажі та наголошує, що у своїй діяльності керується чинними нормативно - правовими та нормативними актами, зокрема тими, які визначають розмір винагороди Оператора, а також повідомляє, що не використовує цінові методи конкуренції.'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text1,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo1)
            bot.send_message(message.from_user.id, _text2,
                             reply_markup=user_markup)
            bot.send_message(message.from_user.id, _text3,
                             reply_markup=user_markup)
        elif 'Продаж спеціальних дозволів на користування надрами' == message.text:
            _text = "Розмір грошової винагороди оператора за придбаний лот встановлюється у відсотковому відношенні до ціни продажу та складає:" + \
                    "\n1 відсоток ціни реалізації лота у разі, коли вона є меншою, ніж 15000000 гривень" + \
                    "\n0,5 відсотка ціни реалізації лота у разі, коли вона становить 15000000 гривень або більше."
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
        elif 'Оренда вагонів Укрзалізниці' == message.text:
            _text = "Розмір грошової винагороди оператора за придбаний лот встановлюється у відсотковому відношенні до ціни продажу та складає 5 (п’ять) відсотків від ціни реалізації, помноженої на кількість вагонів, що міститься у лоті." + \
                    "\nЗа участь в електронних торгах аукціонах) після завершення електронного аукціону з учасників справляється плата за участь у розмірі одного неоподатковуваного мінімуму доходів громадян(17, 00 грн.)."
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
        elif 'Продаж та оренда майна Укрзалізниці' == message.text:
            _text = "Переможець електронного аукціону сплачує організатору електронного аукціону винагороду в розмірі, що становить 1 відсоток ціни продажу майна(активів) або річного розміру орендної плати."
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
        elif 'Банкрутство' == message.text:
            _text = 'Розмір грошової винагороди оператора за придбане майно банкрутів встановлюється у відсотковому відношенні до ціни продажу та складає:'
            photo = open(os.path.join(imagesDir, 'bankrutstvo.jpg'), 'rb')
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo)
        elif 'Продаж окремих партій необробленої деревини' == message.text:
            _text = 'Розмір грошової винагороди оператора за придбану партію необробленої деревини: 1,5 відсотка від ціни реалізації лота. За участь у електронному аукціоні учасники сплачують реєстраційний внесок у наступному розмірі:'
            photo = open(os.path.join(imagesDir, 'derevena.jpg'), 'rb')
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text,
                             reply_markup=user_markup)
            bot.send_photo(message.from_user.id, photo)
        elif message.text == 'Що потрібно для участі в аукціоні?':
            _text = 'Для участі в аукціоні потрібно бути зареєстрованим на майданчику та зняти тестовий режим. У потрібному Вам аукціоні натиснути клавішу «Подати пропозицію», за необхідності вказати закриту цінову пропозицію та проставити погодження з умовами, після чого натиснути клавішу «зберегти». Створити рахунки та оплатити гарантійний та реєстраційний внески та завантажити необхідні документи. Після виконання всіх дій натиснути клавішу «Подати на розгляд».'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Як зареєструватися?':
            _text = 'Для реєстрації на майданчику потрібно в інтерфейсі майданчика натиснути клавішу «Зареєструватися» після чого заповнити поля, проставити позначки погодження та натиснути внизу клавішу «Зареєструватися». Далі потрібно у вкладці «Учасник» обрати тип участі, відповідно до нього заповнити всі поля позначені зірочкою (вони є обов’язковими) та натиснути вгорі клавішу «Зберегти». Ваш кабінет буде в стані «Підготовлено». Для проходження ідентифікації потрібно завантажити необхідні документи (копії паспорту для фізичних осіб) або оплатити 9 грн. за реєстрацію(для юридичних осіб).'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Які документи потрібні для реєстрації?':
            _text = 'Якщо Ви хочете працювати на майданчику як фізична особа або як фізична особа-підприємець, то для проходження ідентифікації Вам буде достатньо завантажити сканкопії паспорту та коду. Якщо Ви хочете працювати як юридична особа, то для проходження ідентифікації Вам потрібно підписати та завантажити скановану копію підписаного договору з майданчиком (договір-оферта) або оплатити 9 грн з розрахункового рахунку вашої компанії.'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Як подати заявку / пропозицію на аукціон?':
            _text = 'В актуальному вам аукціоні натиснути клавішу «Подати пропозицію», прописати закриту цінову пропозицію та проставити позначки підтвердження, після чого натиснути клавішу «зберегти». Створити та оплатити рахунки на оплату гарантійного та реєстраційного внесків, натиснувши клавішу «Створити рахунок» та завантажити необхідні документи. Після виконання всіх дій натиснути клавішу «Подати на розгляд».'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Як подивитися приміщення/авто, ознайомитися із документами?':
            _text = 'На сторінці з інформацією про аукціон Ви також можете переглянути документи аукціону та завантажені фото майна, що продається. Якщо Вас зацікавить більш детальна інформація Ви можете звернутися напряму до замовника – в описі лоту вказана контактна особа.'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Які документи потрібні для участі в аукціоні?':
            _text = 'Для участі в аукціоні з продажу об’єктів малої приватизації потрібно завантажити документи в залежності від типу участі учасника. Перелік документів можна переглянути відкривши вкладку «Документи», що знаходиться на синій панелі внизу сторінки майданчика та обравши свій тип участі. Також радимо уважно ознайомитися з документацією аукціону, там можуть бути вказані додаткові вимоги до учасників.'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Чи можна оплачувати гарантійний внесок через касу/ термінал/ з картки іншої людини?':
            _text = 'Так, оплата через касу та термінал допускається в усіх типах аукціонів окрім малої приватизації, але в платіжному дорученні обов’язково має бути вказано призначення платежу відповідно до рахунку. Оплата з карти/від імені іншої людини не допускається взагалі.'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Де знайти інформацію про дату та час аукціону?':
            _text = 'На сторінці з інформацією про аукціон, в розділі «Основні параметри» є перелічені періоди аукціону, там же зазначена точна дата аукціону.'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Як зайти на аукціон?':
            _text = 'Якщо Ви подали пропозицію для участі в аукціоні та були допущені до нього, то в день аукціону вам в особистий кабінет на майданчику та на електронну пошту, вказану при реєстрації, має прийти індивідуальне посилання.'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == 'Як проходить аукціон?':
            _text = 'Англійський аукціон проходить в три раунди на підвищення ціни. Кожен учасник має три хвилини, щоб зробити ставку. Учасник може залишити початкову цінову пропозицію без змін або ж підняти її не менше ніж крок аукціону.' + \
                    'Голландський аукціон проходить з покроковим зниженням початкової ціни. Система автоматично знижує ціну на крок аукціону.Будь - який учасник, допущений до участі в даному аукціоні може зупинити зниження ціни. Після цього всім учасникам аукціону, крім учасника, що подав цінову пропозицію дається можливість подати закрити цінові пропозиції вищі від поточної ціни лоту. Коли всі закриті цінові пропозиції подані, учасник, що подав цінову пропозицію та зупинив етап покрокового зниження ціни має право однократно зробити цінову пропозицію вищу від поточної ціни лоту не менше ніж на крок аукціону.'
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif message.text == '':
            _text = ''
            user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
            user_markup.row('\U0001F519Головне меню')
            bot.send_message(message.from_user.id, _text, reply_markup=user_markup)
        elif user.t_erdpou == "INPUT":
            check_erdpou(message, user)
        elif user.t_cpv == "INPUT":
            check_cpv(message, user)
        elif user.t_comment > 0:
            add_comment(message, user)
        else:
            bot.send_message(message.from_user.id,
                             'Виберіть будь ласка зі списку або введить /main повернення в головне меню')
    except Exception as e:
        print(e)


def add_comment(message, user):
    comment = Comments()
    comment.subscriber_id = user.id
    comment.text = message.text
    user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
    user_markup.row('\U0001F519Головне меню')
    bot.send_message(message.from_user.id, text=message.text, reply_to_message_id=user.t_comment,
                     reply_markup=user_markup)
    user.t_comment = 0
    db.session.add(comment)
    db.session.commit()


def click_prices(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
    user_markup.row('\U0001F519Головне меню')
    user_markup.row('ЦБД1 (продаж майна неплатоспроможних банків та НБУ)')
    user_markup.row('ЦБД2 (продаж майна державної та комунальної форми власності)')
    user_markup.row('ЦБД2 (продаж майна приватної форми власності)')
    user_markup.row('Продаж майна АТ КБ "ПРИВАТБАНК"')
    user_markup.row('Оренда майна державної, комунальної та приватної форм власності')
    user_markup.row('Мала приватизація державного і комунального майна')
    user_markup.row('Продаж спеціальних дозволів на користування надрами')
    user_markup.row('Оренда вагонів Укрзалізниці')
    user_markup.row('Продаж та оренда майна Укрзалізниці')
    user_markup.row('Банкрутство')
    user_markup.row('Продаж окремих партій необробленої деревини')
    bot.send_message(message.from_user.id, 'Вся інформація про тарифи', reply_markup=user_markup)


def click_subscribe(message):
    user = Subscriber.query.filter_by(chat_id=message.from_user.id).first()
    if user is not None:
        user.t_category = 0
        user.t_state = 0
        db.session.commit()

    user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
    user_markup.row('\U0001F519Головне меню')
    user_markup.row('\U0001F697Авто', '\U0001F3D8Нерухомість')
    user_markup.row('\U0001F3D9Оренда', '\U0001F4C3Мала приватизація')
    user_markup.row('\U0001F527Металобрухт', '\U0001F682Вагони')
    user_markup.row('\U0001F30FЗемельні ділянки', '\U0001F4B0Користування надрами')
    user_markup.row('\U0001F4C1Велика приватизація', '\U0001F3DAПродаж майна банкрутів')
    user_markup.row('\U0001F333Необроблена деревина')
    user_markup.row('\U0001F30AПромисел - Право використання водних біоресурсів')
    user_markup.row('\U0001F522ЄРДПОУ', '\U0001F522CPV')
    bot.send_message(message.from_user.id, 'Виберіть категорию', reply_markup=user_markup)


def select_category(message, user):
    test = message.text[1:]
    category = Category.query.filter_by(title=test).first()
    if user and category:
        user.t_category = category.id
        db.session.commit()
    select_state(message, 0)


def input_erdpou(message, user):
    if user:
        user.t_erdpou = "INPUT"
        db.session.commit()
    markup = telebot.types.ReplyKeyboardRemove(selective=False)
    bot.send_message(message.from_user.id, 'Ви вибрали "ЄДРПОУ". Введіть код ЄДРПОУ', reply_markup=markup)


def input_cpv(message, user):
    if user:
        user.t_cpv = "INPUT"
        db.session.commit()
    markup = telebot.types.ReplyKeyboardRemove(selective=False)
    bot.send_message(message.from_user.id, 'Ви вибрали "CPV". Введіть код CPV', reply_markup=markup)


def check_erdpou(message, user):
    result = re.match(r'^(\d{8}|\d{10})$', message.text)
    if user:
        if result:
            user.t_erdpou = message.text
            db.session.commit()
            select_state(message, 0)
            select_state(message, 0)
        else:
            bot.send_message(message.from_user.id,
                             'Некоректно введений код ЄДРПОУ. Цей код складається з восьми або десяти цифр. Будь ласка введіть правильний код, або наберіть /main для повернення до головного меню')


def check_cpv(message, user):
    result = re.match(r'^(\d{8}-\d{1})$', message.text)
    if user:
        if result:
            user.t_cpv = message.text
            db.session.commit()
            select_state(message, 0)
            select_state(message, 0)
        else:
            bot.send_message(message.from_user.id,
                             'Некоректно введений код CPV. Цей код має такий вигляд #######-# де # це цифра від 0 до 9. Будь ласка введіть правильний код, або наберіть /main для повернення до головного меню')


def select_state(message, page):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, False)
    if page == 0:
        user_markup.row('всі регіони', 'Вінницька', 'Волинська')
        user_markup.row('Дніпропетровська', 'Донецька', 'Житомирська')
        user_markup.row('Закарпатська', 'Запорізька', 'Івано-Франківська')
        user_markup.row('\U0001F519Головне меню', '\U0001F504інші')
    if page == 1:
        user_markup.row('Київська', 'Кіровоградська', 'Луганська')
        user_markup.row('Львівська', 'Миколаївська', 'Одеська')
        user_markup.row('Полтавська', 'Рівненська', 'Сумська')
        user_markup.row('\U0001F502назад', '\U0001F519Головне меню', '\U0001F504ще')
    if page == 2:
        user_markup.row('Тернопільська', 'Харківська', 'Херсонська')
        user_markup.row('Хмельницька', 'Черкаська', 'Чернігівська')
        user_markup.row('Чернівецька')
        user_markup.row('\U0001F519Головне меню', '\U0001F502назад', )

    bot.send_message(message.from_user.id, 'Будь ласка виберіть регіон', reply_markup=user_markup)


def make_subscribe(message, user):
    state = State.query.filter_by(name=message.text).first()
    m_text = 'Ви вже підписані на цю розсилку'
    if user:
        if user.t_erdpou is None and user.t_cpv is None:
            r_theme = Theme.query.filter_by(state_id=state.id, category_id=user.t_category,
                                            subscriber_id=user.id).first()
            if r_theme is None:
                theme = Theme()
                theme.state_id = state.id
                theme.category_id = user.t_category
                theme.subscriber_id = user.id
                theme.last_update = datetime.now()
                db.session.add(theme)
                db.session.commit()
                category = Category.query.filter_by(id=user.t_category).first()
                m_text = 'Ви підписалися на ' + category.title + ' - ' + state.name
        elif user.t_erdpou is not None:
            r_theme = ThemeErdpou.query.filter_by(state_id=state.id, erdpou=user.t_erdpou,
                                                  subscriber_id=user.id).first()
            if r_theme is None:
                theme_er = ThemeErdpou()
                theme_er.state_id = state.id
                theme_er.erdpou = user.t_erdpou
                theme_er.subscriber_id = user.id
                theme_er.last_update = datetime.now()
                db.session.add(theme_er)
                m_text = 'Ви підписалися на ЄДРПОУ: ' + user.t_erdpou + ' - ' + state.name
                user.t_erdpou = None
                db.session.commit()
        elif user.t_cpv is not None:
            r_theme = ThemeCPV.query.filter_by(state_id=state.id, cpv=user.t_cpv,
                                               subscriber_id=user.id).first()
            if r_theme is None:
                theme_cpv = ThemeCPV()
                theme_cpv.state_id = state.id
                theme_cpv.cpv = user.t_cpv
                theme_cpv.subscriber_id = user.id
                theme_cpv.last_update = datetime.now()
                db.session.add(theme_cpv)
                m_text = 'Ви підписалися на CPV: ' + user.t_cpv + ' - ' + state.name
                user.t_cpv = None
                db.session.commit()

    user_markup = telebot.types.ReplyKeyboardMarkup(True, False, selective=True)
    user_markup.row('\U0001F519Головне меню')
    bot.send_message(message.from_user.id, m_text,
                     reply_markup=user_markup)


def remove_subscribe(message, user):
    params = message.text.split(' - ')
    state = State.query.filter_by(name=params[1]).first()
    if 'ЄДРПОУ' in params[0]:
        tmp = params[0].split(': ')
        erdpou = tmp[1].strip()
        if erdpou and state and user:
            r_theme = ThemeErdpou.query.filter_by(state_id=state.id, erdpou=erdpou, subscriber_id=user.id).first()
            if r_theme:
                db.session.delete(r_theme)
                db.session.commit()
    elif 'CPV' in params[0]:
        tmp = params[0].split(': ')
        cpv = tmp[1].strip()
        if cpv and state and user:
            r_theme = ThemeCPV.query.filter_by(state_id=state.id, cpv=cpv, subscriber_id=user.id).first()
            if r_theme:
                db.session.delete(r_theme)
                db.session.commit()
    else:
        category = Category.query.filter_by(title=params[0]).first()
        if category and state and user:
            r_theme = Theme.query.filter_by(state_id=state.id, category_id=category.id, subscriber_id=user.id).first()
            if r_theme:
                db.session.delete(r_theme)
                db.session.commit()

    show_subscribes(message, user, 2)


def show_subscribes(message, user, type):
    db.session.commit()
    themes = user.themes
    themes_erdpou = user.themes_erdpou
    themes_cpv = user.themes_cpv
    user_markup = telebot.types.ReplyKeyboardMarkup(True, False, selective=True)
    user_markup.row('\U0001F519Головне меню')
    m = 'Ви підписані на наступні категорії: \n'
    for t in themes:
        if type == 1:
            m += t.category.title + ' - ' + t.state.name + ' область\n'
        if type == 2:
            user_markup.row(t.category.title + ' - ' + t.state.name)
    for te in themes_erdpou:
        if type == 1:
            m += 'ЄДРПОУ: ' + te.erdpou + ' - ' + te.state.name + '\n'
        if type == 2:
            user_markup.row('ЄДРПОУ: ' + te.erdpou + ' - ' + te.state.name)

    for tcp in themes_cpv:
        if type == 1:
            m += 'CPV: ' + tcp.cpv + ' - ' + tcp.state.name + '\n'
        if type == 2:
            user_markup.row('CPV: ' + tcp.cpv + ' - ' + tcp.state.name)

    if themes is None and themes_erdpou is None and themes_cpv is None:
        bot.send_message(message.from_user.id, 'У вас немає активних підписок',
                         reply_markup=user_markup)
    else:
        bot.send_message(message.from_user.id, m,
                         reply_markup=user_markup)


def find_subscribe(sub):
    if sub.state.id > 1:
        auctions = Auction.query.filter(Auction.category_id == sub.category.id,
                                        Auction.state_id == sub.state.id).order_by(desc(Auction.created_date)).limit(
            5).all()
    else:
        auctions = Auction.query.filter(Auction.category_id == sub.category.id, Auction.state_id >= 1).order_by(
            desc(Auction.created_date)).limit(5).all()

    cnt = 1
    if len(auctions) > 0:
        message_text = '*' + sub.category.title + ' - ' + (
            sub.state.name + '*' if sub.state_id == 1 else sub.state.name + ' область*') + '\n'
        for auction in auctions:
            state_info = ''
            if auction.state_id > 1:
                state_info = '(' + auction.state.name + ' область)'
            message_text += str(cnt) + '. [' + ((auction.title[:50] + '..]') if len(
                auction.title) > 50 else '[' + auction.title + ']') + '(https://sale.tbe-birzha.com.ua' + auction.url + ') ' + state_info + '\n'
            cnt += 1

        try:
            bot.send_message(sub.subscriber.chat_id, text=message_text)
        except Exception as e:
            print(e)


def find_erdpou(sub):
    if sub.state.id > 1:
        auctions = Auction.query.filter(Auction.erdpou == sub.erdpou, Auction.state_id == sub.state_id).order_by(
            desc(Auction.created_date)).limit(5).all()
    else:
        auctions = Auction.query.filter(Auction.erdpou == sub.erdpou, Auction.state_id >= 1).order_by(
            desc(Auction.created_date)).limit(5).all()
    cnt = 1
    if len(auctions) > 0:
        message_text = '*ЄДРПОУ: ' + sub.erdpou + ' - ' + sub.state.name + '*\n'
        for auction in auctions:
            state_info = ''
            if auction.state_id > 1:
                state_info = '(' + auction.state.name + ' область)'
            message_text += str(cnt) + '. [' + ((auction.title[:50] + '..]') if len(
                auction.title) > 50 else '[' + auction.title + ']') + '(https://sale.tbe-birzha.com.ua' + auction.url + ') ' + state_info + '\n'
            cnt += 1

        try:
            bot.send_message(sub.subscriber.chat_id, text=message_text)
        except Exception as e:
            print(e)


def find_cpv(sub):
    if sub.state.id > 1:
        auctions = Auction.query.filter(Auction.cpv_code == sub.cpv, Auction.state_id == sub.state_id).order_by(
            desc(Auction.created_date)).limit(5).all()
    else:
        auctions = Auction.query.filter(Auction.cpv_code == sub.cpv, Auction.state_id >= 1).order_by(
            desc(Auction.created_date)).limit(5).all()
    cnt = 1
    if len(auctions) > 0:
        message_text = '*CPV: ' + sub.cpv + ' - ' + sub.state.name + '*\n'
        for auction in auctions:
            state_info = ''
            if auction.state_id > 1:
                state_info = '(' + auction.state.name + ' область)'
            message_text += str(cnt) + '. [' + ((auction.title[:50] + '..]') if len(
                auction.title) > 50 else '[' + auction.title + ']') + '(https://sale.tbe-birzha.com.ua' + auction.url + ') ' + state_info + '\n'
            cnt += 1

        try:
            bot.send_message(sub.subscriber.chat_id, text=message_text)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    bot.polling()
