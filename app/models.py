import datetime
from app import db, login, app
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class User(UserMixin, db.Model):
    __tablename__ = 't_users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), index=True)
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Boolean)
    is_active = db.Column(db.Boolean)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def to_dict(self):
        r = {
            'id': self.id,
            'email': self.email,
            'is_admin': self.is_admin is not None and self.is_admin,
            'is_active': self.is_active is not None and self.is_active
        }
        return r

    def from_dict(self, user):
        self.is_admin = user["admin"]
        self.is_active = user["active"]

    def __repr__(self):
        return '<User {}>'.format(self.username)


class ThemeErdpou(db.Model):
    __tablename__ = 't_theme_erdpou'
    id = db.Column(db.Integer, primary_key=True)
    erdpou = db.Column(db.String(128), index=True)
    state_id = db.Column(db.Integer, db.ForeignKey('t_state.id'), index=True)
    subscriber_id = db.Column(db.Integer, db.ForeignKey('t_subscriber.id'), index=True)
    last_update = db.Column(db.DateTime(), index=True)


class ThemeCPV(db.Model):
    __tablename__ = 't_theme_cpv'
    id = db.Column(db.Integer, primary_key=True)
    cpv = db.Column(db.String(128), index=True)
    state_id = db.Column(db.Integer, db.ForeignKey('t_state.id'), index=True)
    subscriber_id = db.Column(db.Integer, db.ForeignKey('t_subscriber.id'), index=True)
    last_update = db.Column(db.DateTime(), index=True)


class Comments(db.Model):
    __tablename__ = 't_comments'
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    subscriber_id = db.Column(db.Integer, db.ForeignKey('t_subscriber.id'), index=True)


class ScraperLog(db.Model):
    __tablename__ = 't_scraper_log'
    id = db.Column(db.Integer, primary_key=True)
    result_code = db.Column(db.SmallInteger)
    error = db.Column(db.String(2048))
    up_date = db.Column(db.DateTime(), index=True)


class Theme(db.Model):
    __tablename__ = 't_theme'
    id = db.Column(db.Integer, primary_key=True)
    category_id = db.Column(db.Integer, db.ForeignKey('t_category.id'), index=True)
    state_id = db.Column(db.Integer, db.ForeignKey('t_state.id'), index=True)
    subscriber_id = db.Column(db.Integer, db.ForeignKey('t_subscriber.id'), index=True)
    last_update = db.Column(db.DateTime(), index=True)

    def to_dict(self):
        data = {
            'id': self.id,
            'category': self.category.title,
            'state': self.state.name
        }
        return data

    def __repr__(self):
        return '<Theme {}>'.format(self.id)


class Subscriber(db.Model):
    __tablename__ = 't_subscriber'
    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.String(128), index=True)
    username = db.Column(db.String(128), index=True)
    t_category = db.Column(db.Integer)
    t_erdpou = db.Column(db.String(128))
    t_cpv = db.Column(db.String(128))
    t_comment = db.Column(db.Integer)
    themes = db.relationship("Theme", backref='subscriber', lazy='dynamic', cascade="all, delete-orphan")
    themes_erdpou = db.relationship("ThemeErdpou", backref='subscriber', lazy='dynamic', cascade="all, delete-orphan")
    themes_cpv = db.relationship("ThemeCPV", backref='subscriber', lazy='dynamic', cascade="all, delete-orphan")
    comment = db.relationship("Comments", backref='subscriber', lazy='dynamic', cascade="all, delete-orphan")
    created_date = db.Column(db.DateTime, default=datetime.datetime.now())

    def to_dict(self):

        tms = ''
        for t in self.themes:
            tms += t.category.title + ' (' + t.state.name + ') '

        url = ""
        if self.username:
            url = "https://t.me/" + self.username

        data = {
            'id': self.id,
            'url': url,
            'uid': self.chat_id,
            'name': self.username,
            'themes': tms,
            'createDate': self.created_date.strftime("%Y-%m-%d %H:%M")
        }

        return data

    def __repr__(self):
        return '<Subscriber {}>'.format(self.username)


class Category(db.Model):
    __tablename__ = 't_category'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128), index=True)
    name = db.Column(db.String(128), index=True)
    url = db.Column(db.String(512))
    site = db.Column(db.Integer)
    cpv_codes = db.Column(db.Text)
    themes = db.relationship("Theme", backref='category', lazy='dynamic', cascade="all, delete-orphan")
    auctions = db.relationship('Auction', backref='category', lazy='dynamic', cascade="all, delete-orphan")

    def __repr__(self):
        return '<Category {}>'.format(self.title)


class State(db.Model):
    __tablename__ = 't_state'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True)
    pars_name = db.Column(db.String(128), index=True)
    themes = db.relationship("Theme", backref='state', lazy='dynamic', cascade="all, delete-orphan")
    erdpou = db.relationship("ThemeErdpou", backref='state', lazy='dynamic', cascade="all, delete-orphan")
    cpv = db.relationship("ThemeCPV", backref='state', lazy='dynamic', cascade="all, delete-orphan")
    auctions = db.relationship('Auction', backref='state', lazy='dynamic', cascade="all, delete-orphan")

    def __repr__(self):
        return '<State {}>'.format(self.name)


class Auction(db.Model):
    __tablename__ = 't_auctions'
    id = db.Column(db.Integer, primary_key=True)
    category_id = db.Column(db.Integer, db.ForeignKey('t_category.id'), nullable=True)
    state_id = db.Column(db.Integer, db.ForeignKey('t_state.id'), nullable=True)
    aid = db.Column(db.String(128), index=True, unique=True)
    erdpou = db.Column(db.String(128), index=True)
    cpv_code = db.Column(db.String(128), index=True)
    title = db.Column(db.String(1024))
    url = db.Column(db.String(512))
    created_date = db.Column(db.DateTime, default=datetime.datetime.now())

    def to_dict(self):
        data = {
            'id': self.id,
            'url': self.url,
            'title': self.title,
            'aid': self.aid,
            'erdpou': self.erdpou,
            'cpv_code': self.cpv_code,
            'category_id': self.category_id,
            'category': self.category.title,
            'state_id': self.state_id,
            'state': self.state.name,
            'createDate': self.created_date.strftime("%Y-%m-%d %H:%M")

        }

        return data

    def __repr__(self):
        return '<Auction {}>'.format(self.aid)
