# TBE telegram bot

Telegram bot for European commodity exchange

## Installation for development

Use the [docker](https://towardsdatascience.com/connect-to-mysql-running-in-docker-container-from-a-local-machine-6d996c574e55) for mysql 

```bash
> docker pull mysql/mysql-server:latest
```

```bash
> docker volume create mysql-volume
mysql-volume
```

```bash
> docker run --name=tbe-bot-mysql -p3306:3306 -v mysql-volume:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -d mysql/mysql-server:latest
```

```bash
> docker exec -it mk-mysql bash
bash-4.2#
```

```bash
mysql> update mysql.user set host = ‘%’ where user=’root’;
mysql> FLUSH PRIVILEGES;
```

