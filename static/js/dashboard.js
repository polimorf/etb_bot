var chartData = {
    labels: ["S", "M", "T", "W", "T", "F", "S"],
    datasets: [{ data: [58, 44, 48, 50, 68, 69, 63]}]
};


var app = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data() {
        return {
            subs: 0,
            subs_d: 0,
            subs_w: 0,
            subs_m: 0,
            act: 0,
            act_d: 0,
            act_w: 0,
            act_m: 0
        };
    },
    methods: {
        submit: function (event) {
        }
    },
    computed:
        {}
    ,
    mounted() {
        axios.get('/api/v1/dashboard')
            .then(function (responce) {
                app.$data.subs = responce.data.subs;
                app.$data.act = responce.data.act;
            })
            .catch(function (error) {
                console.log(error);
            })
    }
});

var chLine = document.getElementById("chLine");
if (chLine) {
    new Chart(chLine, {
        type: 'line',
        data: chartData,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            },
            legend: {
                display: false
            }
        }
    });
}

$(document).ready(function () {
    console.log('dashboard is ready');
});



