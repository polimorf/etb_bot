from flask import render_template, flash, redirect, url_for, request, abort
from flask_login import current_user, login_user, logout_user
from app.models import User
from app import app, db
from app.forms import LoginForm, RegisterForm
import datetime

# from telebot import types

version = '1.1.1'


# @app.route('/1075674805:AAHwS8URaqHOSfAMQFYDus2R_WjIDfDqBtU', methods=['POST'])
# def webhook():
#    if request.headers.get('content-type') == 'application/json':
#        json_string = request.get_data().decode('utf-8')
#        update = types.Update.de_json(json_string)
#        bot.process_new_updates([update])
#        return ''
#    else:
#        abort(403)


@app.route('/')
@app.route('/dashboard')
def dashboard():
    if not (current_user.is_authenticated and current_user.is_active):
        return redirect(url_for('login'))
    else:
        return render_template('main.html', type="dashboard", user=current_user, version=version)


@app.route('/auctions')
def auctions():
    if not (current_user.is_authenticated and current_user.is_active):
        return redirect(url_for('login'))
    else:
        return render_template('main.html', type="auctions", user=current_user, version=version)


@app.route('/subscribers')
def subscribers():
    if not (current_user.is_authenticated and current_user.is_active):
        return redirect(url_for('login'))
    else:
        return render_template('main.html', type="subscribers", user=current_user, version=version)


@app.route('/settings')
def settings():
    if not (current_user.is_authenticated and current_user.is_active):
        return redirect(url_for('login'))
    else:
        return render_template('main.html', type="settings", user=current_user, version=version)


@app.route('/broadcast')
def broadcast():
    if not (current_user.is_authenticated and current_user.is_active):
        return redirect(url_for('login'))
    else:
        return render_template('main.html', type="broadcast", user=current_user, version=version)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated and current_user.is_active:
        return redirect(url_for('dashboard'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data, duration=datetime.timedelta(days=30))
        return redirect(url_for('dashboard'))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))
