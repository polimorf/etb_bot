-- --------------------------------------------------------
-- Хост:                         192.168.33.20
-- Версия сервера:               5.7.31-0ubuntu0.16.04.1 - (Ubuntu)
-- Операционная система:         Linux
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица tbe.t_state
CREATE TABLE IF NOT EXISTS `t_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `pars_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_t_state_name` (`name`),
  KEY `ix_t_state_pars_name` (`pars_name`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы tbe.t_state: ~0 rows (приблизительно)
DELETE FROM `t_state`;
/*!40000 ALTER TABLE `t_state` DISABLE KEYS */;
INSERT INTO `t_state` (`id`, `name`, `pars_name`) VALUES
	(1, 'всі регіони', 'NONE'),
	(2, 'Вінницька', 'Вінниц'),
	(3, 'Волинська', 'Волин'),
	(4, 'Дніпропетровська', 'Дніпро'),
	(5, 'Донецька', 'Донецьк'),
	(6, 'Житомирська', 'Житомир'),
	(7, 'Закарпатська', 'Закарпат'),
	(8, 'Запорізька', 'Запорі'),
	(9, 'Івано-Франківська', 'Івано-Франківськ'),
	(10, 'Київська', 'Київ'),
	(11, 'Кіровоградська', 'Кіровоград'),
	(12, 'Луганська', 'Луганськ'),
	(13, 'Львівська', 'Львів'),
	(14, 'Миколаївська', 'Миколаїв'),
	(15, 'Одеська', 'Одес'),
	(16, 'Полтавська', 'Полтав'),
	(17, 'Рівненська', 'Рівненська'),
	(18, 'Сумська', 'Сумська'),
	(19, 'Тернопільська', 'Тернопіль'),
	(20, 'Харківська', 'Харків'),
	(21, 'Херсонська', 'Херсон'),
	(22, 'Хмельницька', 'Хмельницьк'),
	(23, 'Черкаська', 'Черкаська'),
	(24, 'Чернігівська', 'Чернігів'),
	(25, 'Чернівецька', 'Чернівецька');
/*!40000 ALTER TABLE `t_state` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
