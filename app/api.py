from app import restapi, db
from flask_restful import Resource, reqparse
from app.models import Auction, Subscriber
from sqlalchemy import desc

prs = reqparse.RequestParser()


class AuctionsApi(Resource):
    def get(self, page):
        result = {"pages": [], "auctions": []}
        t_query = Auction.query.order_by(desc(Auction.created_date)).paginate(page, 20, False)
        pages = []
        for p in t_query.iter_pages():
            link = {"active": False, "text": "0", "num": 0}
            if p:
                if p != t_query.page:
                    link["text"] = str(p)
                    link["num"] = p
                else:
                    link["active"] = True
                    link["text"] = str(p)
            else:
                link["text"] = "..."
            pages.append(link)
        result["pages"] = pages
        record_items = t_query.items
        for r in record_items:
            result["auctions"].append(r.to_dict())
        return result


class SubscriberApi(Resource):
    def get(self, page):
        result = {"pages": [], "subscribers": []}
        t_query = Subscriber.query.paginate(page, 20, False)
        pages = []
        for p in t_query.iter_pages():
            link = {"active": False, "text": "0", "num": 0}
            if p:
                if p != t_query.page:
                    link["text"] = str(p)
                    link["num"] = p
                else:
                    link["active"] = True
                    link["text"] = str(p)
            else:
                link["text"] = "..."
            pages.append(link)
        result["pages"] = pages
        record_items = t_query.items
        for r in record_items:
            result["subscribers"].append(r.to_dict())
        return result


class DashboardApi(Resource):
    def get(self):
        result = {"subs": 0, "subs_d": 0,
                  "subs_w": 0, "subs_m": 0,
                  "act": 0, "act_d": 0,
                  "act_w": 0, "act_m": 0}

        result["subs"] = db.session.query(Subscriber).count()
        result["act"] = db.session.query(Auction).count()

        return result


restapi.add_resource(AuctionsApi, '/api/v1/auctions/<int:page>')
restapi.add_resource(SubscriberApi, '/api/v1/subscribers/<int:page>')
restapi.add_resource(DashboardApi, '/api/v1/dashboard')
